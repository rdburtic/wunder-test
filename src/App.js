import React, { Component } from 'react';
import './App.scss';
import MainForm from './components/MainForm';
import { Container } from 'semantic-ui-react';
import logo from './assets/wunder_mobility_white.svg';

class App extends Component {

  render() {
    return(
      <Container textAlign='center' className="mainContainer">
        <img src={logo} alt="Wunder Logo" className="logo" />
        <MainForm />
      </Container>
    )
  }

}

export default App;
