import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';

class AddressInformation extends Component {

    next = (e) => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }

    render(){
        const { values } = this.props;
        return(
            <Form onSubmit={this.next} className="formContainer">
                <h1 className="ui centered formStepTitle">Insert Address Information</h1>
                <h2 className="ui centered formStep"> 2 / 3</h2>
                <Form.Field>
                    <label>Street</label>
                    <input
                    placeholder='Street'
                    onChange={this.props.handleChange('street')}
                    defaultValue={values.street}
                    className="formInput"
                    required
                    />
                </Form.Field>
                <Form.Field>
                    <label>House Number</label>
                    <input
                    placeholder='House Number'
                    onChange={this.props.handleChange('houseNumber')}
                    defaultValue={values.houseNumber}
                    className="formInput"
                    required
                    />
                </Form.Field>
                <Form.Field>
                    <label>Zip Code</label>
                    <input
                    placeholder='Zip Code'
                    onChange={this.props.handleChange('zipCode')}
                    defaultValue={values.zipCode}
                    className="formInput"
                    required
                    />
                </Form.Field>
                <Form.Field>
                    <label>City</label>
                    <input
                    placeholder='City'
                    onChange={this.props.handleChange('city')}
                    defaultValue={values.city}
                    className="formInput"
                    required
                    />
                </Form.Field>
                <Button onClick={this.back}>Back</Button>
                <Button type='submit' className='next'>Next</Button>
            </Form>
        )
    }
}

export default AddressInformation;
