import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';

class PersonalInformation extends Component {

    saveAndContinue = (e) => {
        e.preventDefault();
        this.props.nextStep();
    }

    render(){
        const { values } = this.props;
        return(
            <Form onSubmit={this.saveAndContinue} className="formContainer">
                <h1 className="ui centered formStepTitle">Insert Personal Information</h1>
                <h2 className="ui centered formStep"> 1 / 3</h2>
                <Form.Field>
                    <label>First Name</label>
                    <input
                    placeholder='First Name'
                    onChange={this.props.handleChange('firstName')}
                    defaultValue={values.firstName}
                    className="formInput"
                    required
                    />
                </Form.Field>
                <Form.Field>
                    <label>Last Name</label>
                    <input
                    placeholder='Last Name'
                    onChange={this.props.handleChange('lastName')}
                    defaultValue={values.lastName}
                    className="formInput"
                    required
                    />
                </Form.Field>
                <Form.Field>
                    <label>Telephone</label>
                    <input
                    type='tel'
                    placeholder='Telephone'
                    onChange={this.props.handleChange('telephone')}
                    defaultValue={values.telephone}
                    className="formInput"
                    required
                    />
                </Form.Field>
                <Button type='submit' className='next'>Next</Button>
            </Form>
        )
    }
}

export default PersonalInformation;
