import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';
import axios from 'axios';

class AddressInformation extends Component {

    state = {
        errorEncountered: false
    }
    submit = (e) => {
        e.preventDefault()
        axios.post('/default/wunderfleet-recruiting-backend-dev-save-payment-data', 
        JSON.stringify({customerId: 1, iban: this.props.values.iban, owner: this.props.values.accountOwner})).then(response => {
            if(response.status === 200) {
                this.props.setPaymentDataId(response.data.paymentDataId);
                localStorage.removeItem("userData");
                this.simulateDatabaseSave();
                this.props.nextStep();
            }
        }).catch( error => {
            this.setState({
                errorEncountered: true
            });
        });
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }

    simulateDatabaseSave = () => {
        const userData = {
            firstName: this.props.values.firstName,
            lastName: this.props.values.statelastName,
            telephone: this.props.values.telephone,
            street: this.props.values.street,
            houseNumber: this.props.values.houseNumber,
            zipCode: this.props.values.zipCode,
            city: this.props.values.city,
            accountOwner: this.props.values.accountOwner,
            iban: this.props.values.iban,
            paymentDataId: this.props.values.paymentDataId
        }
        const database = JSON.parse(localStorage.getItem("simulatedDatabase"));
        database.push(userData);
        localStorage.setItem("simulatedDatabase", JSON.stringify(database));
    }

    render(){
        const { values } = this.props;
        return(
        <div>
            <Form onSubmit={this.submit} className="formContainer">
                <h1 className="ui centered formStepTitle">Insert Payment Information</h1>
                <h2 className="ui centered formStep"> 3 / 3</h2>
                <Form.Field>
                    <label>Account Owner</label>
                    <input
                    placeholder='Account Owner'
                    onChange={this.props.handleChange('accountOwner')}
                    defaultValue={values.accountOwner}
                    className="formInput"
                    required
                    />
                </Form.Field>
                <Form.Field>
                    <label>IBAN</label>
                    <input
                    placeholder='IBAN'
                    onChange={this.props.handleChange('iban')}
                    defaultValue={values.iban}
                    className="formInput"
                    required
                    />
                </Form.Field>
                <Button onClick={this.back}>Back</Button>
                <Button type='submit' className="next">Submit</Button>
            </Form>
            {this.state.errorEncountered && 
                <h2>An error occured while submitting your data.</h2>
            }  
        </div>
        )
    }
}

export default AddressInformation;
