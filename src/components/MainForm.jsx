import React, { Component } from 'react';
import PersonalInformation from './PersonalInformation';
import AddressInformation from './AddressInformation';
import PaymentInformation from './PaymentInformation';
import Success from './Success';



class MainForm extends Component {
    constructor(props){
        super(props);
        this.state = this.getDataFromLocalStorage() || this.defaultState;
        const database = localStorage.getItem("simulatedDatabase");
        if( database === null) {
            localStorage.setItem("simulatedDatabase", JSON.stringify([]));
        }
    }
    
    defaultState = {
        step: 1,
        firstName: '',
        lastName: '',
        telephone: '',
        street: '',
        houseNumber: '',
        zipCode: '',
        city: '',
        accountOwner: '',
        iban: '',
        paymentDataId: ''
    };
    
    nextStep = () => {
        const { step } = this.state
        this.setState({
            step : step + 1
        },function() {
            localStorage.setItem("userData", JSON.stringify(this.state));
        });
    }

    prevStep = () => {
        const { step } = this.state
        this.setState({
            step : step - 1
        });
    }

    getDataFromLocalStorage = () => {
        const localStorageString = localStorage.getItem("userData");
        if(localStorageString != null) {
            return JSON.parse(localStorageString);
        }
        return false;
    }

    setPaymentDataId = responsePaymentDataId => {
        this.setState({ paymentDataId: responsePaymentDataId });
    }

    handleChange = input => event => {
        this.setState({ [input] : event.target.value }, function() {
            localStorage.setItem("userData", JSON.stringify(this.state));
        });
        
    }

    render(){
        const { step } = this.state;
        const { firstName, lastName, telephone, street, houseNumber, zipCode, city, accountOwner, iban, paymentDataId } = this.state;
        const values = { firstName, lastName, telephone, street, houseNumber, zipCode, city, accountOwner, iban, paymentDataId };
        switch(step) {
        case 1:
            return <PersonalInformation 
                    nextStep={this.nextStep} 
                    handleChange = {this.handleChange}
                    values={values}
                    />
        case 2:
            return <AddressInformation 
                    nextStep={this.nextStep}
                    prevStep={this.prevStep}
                    handleChange = {this.handleChange}
                    values={values}
                    />
        case 3:
            return <PaymentInformation 
                    nextStep={this.nextStep}
                    prevStep={this.prevStep}
                    handleChange = {this.handleChange}
                    values={values}
                    setPaymentDataId = {this.setPaymentDataId}
                    />
        case 4:
            return <Success 
                    values={values}
                    />
        default:
            return <PersonalInformation 
            nextStep={this.nextStep} 
            handleChange = {this.handleChange}
            values={values}
            />
        }
    }
}

export default MainForm;