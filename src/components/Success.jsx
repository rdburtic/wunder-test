import React, { Component } from 'react';

class Success extends Component {

  render() {
    return(
        <div>
          <h1 className="ui centered">Success</h1>
          <h2 className="ui centered">Your payment id is : {this.props.values.paymentDataId}</h2>
        </div>
    )
  }

}

export default Success;
