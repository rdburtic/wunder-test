Task Approach: 

As I have mentioned in the previous interview I mainly work with Sapui5 currently. I have done some freelance projects in the past, one was actually a react native app on which 
I helped out but that one was set up (boileplate wise etc) by someone else, I mainly helped out with a few views and some styling. Since I like to keep up with current tech-stacks
and love tackling new challenges, I decided that even though my experience with React is limited I would build this example application in React to showcase my ability to adapt
to a new technology with relative ease. As such there may be some improvement to how the entire project is structured but for a small example application I hope this will be enough.

This file shouldve contained answers to two questions "Describe possible optimizations for your code" and "Which things could be done better, than you’ve done it?".
I feel as if in this case there is overlap between the two questions and as such I will just list possible things that I would have done different/better which would have also
resulted in a more optimized code. 

    a) Considering the small nature of the application I felt as if going for a full Flux design which is recommended with React or trying to adopt a MVC pattern would cause a 
    lot of overhead. For the sake of structure I did use a MainForm view in which contains shared functionality that is passed down to the child views, which here acts as a combination
    of the Flux pattern recommended Store + Dispatcher. In a bigger application you would have to split all of these for better supporting the creation of multiple stores, for 
    code readability, reusability and just general cleaner design.
    b) In the same spirit as the above remark, I have kept the CSS in just two files (index.scss for the application wide styles and the other for child view specific things). 
    Normally I would have made a smaller scss file for every child view but again here that would have been added overhead (also in this specific case the styling between the child
    views is shared so there would have been no need for the split files).
    c) The code that deals with simulating the database save (and if it would have been an actual database) should have been separated from the files in its own service.
    d) After paying a bit more attention to your design I realized it might have been easier to replicate with Materialize.css instead of Semantic-UI (at least the from 
    animation is a lot closer to yours in the case of Materialize).
    e) I could have probably very easily implemented the API call with base javascript but I wanted to use axios to prove understanding of variety of tools to ease API interaction.

If you have any other questions I would gladly answer them.

